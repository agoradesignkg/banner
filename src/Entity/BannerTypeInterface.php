<?php

namespace Drupal\banner\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Defines the interface for banner types.
 */
interface BannerTypeInterface extends ConfigEntityInterface, EntityDescriptionInterface {

}
