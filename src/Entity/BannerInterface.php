<?php

namespace Drupal\banner\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Defines the interface for banner entities.
 */
interface BannerInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface, EntityPublishedInterface {

  /**
   * Gets the banner title.
   *
   * @return string
   *   The banner title.
   */
  public function getTitle();

  /**
   * Sets the banner title.
   *
   * @param string $title
   *   The banner title.
   *
   * @return $this
   */
  public function setTitle($title);

  /**
   * Gets the banner text.
   *
   * @return string
   *   The banner text.
   */
  public function getText();

  /**
   * Sets the banner text.
   *
   * @param string $text
   *   The banner text.
   *
   * @return $this
   */
  public function setText($text);

  /**
   * Gets the media entity.
   *
   * @return \Drupal\media\MediaInterface|null
   *   The media entity, or null.
   */
  public function getMedia();

  /**
   * Gets the image file behind the referenced media, if available.
   *
   * @return \Drupal\file\Entity\File|null
   *   An image file entity, or null.
   */
  public function getImage();

  /**
   * Gets whether the banner has an image media set.
   *
   * @return bool
   *   TRUE if the banner has an image media set, FALSE otherwise.
   */
  public function hasImage();

  /**
   * Gets the video file behind the referenced media, if available.
   *
   * @return \Drupal\file\Entity\File|null
   *   A video file entity, or null.
   */
  public function getVideo();

  /**
   * Gets whether the banner has an video media set.
   *
   * @return bool
   *   TRUE if the banner has an video media set, FALSE otherwise.
   */
  public function hasVideo();

  /**
   * Gets the custom CSS classes.
   *
   * @return string[]
   *   The custom CSS classes.
   */
  public function getCustomCssClasses();

  /**
   * Sets the custom CSS classes.
   *
   * @param string[] $custom_css_classes
   *   The custom CSS classes.
   *
   * @return $this
   */
  public function setCustomCssClasses(array $custom_css_classes);

  /**
   * Gets whether the banner has custom CSS classes.
   *
   * @return bool
   *   TRUE if the banner has custom CSS classes, FALSE otherwise.
   */
  public function hasCustomCssClasses();

  /**
   * Gets whether the banner has a specific custom CSS class set.
   *
   * @param string $class
   *   The CSS class name to search for.
   *
   * @return bool
   *   TRUE if the banner has the request custom CSS class set, FALSE otherwise.
   */
  public function hasCustomCssClass($class);

  /**
   * Gets the creation timestamp.
   *
   * @return int
   *   The creation timestamp.
   */
  public function getCreatedTime();

  /**
   * Sets the creation timestamp.
   *
   * @param int $timestamp
   *   The creation timestamp.
   *
   * @return $this
   */
  public function setCreatedTime($timestamp);

}
