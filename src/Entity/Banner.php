<?php

namespace Drupal\banner\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\media\Plugin\media\Source\Image;
use Drupal\media\Plugin\media\Source\VideoFile;
use Drupal\user\UserInterface;

/**
 * Defines the banner entity class.
 *
 * @ContentEntityType(
 *   id = "banner",
 *   label = @Translation("Banner"),
 *   label_singular = @Translation("Banner"),
 *   label_plural = @Translation("Banner"),
 *   label_count = @PluralTranslation(
 *     singular = "@count banner",
 *     plural = "@count banner",
 *   ),
 *   bundle_label = @Translation("Banner type"),
 *   handlers = {
 *     "storage" = "Drupal\banner\Storage\BannerStorage",
 *     "access" = "Drupal\banner\Access\BannerAccessControlHandler",
 *     "view_builder" = "Drupal\banner\BannerViewBuilder",
 *     "list_builder" = "Drupal\banner\BannerListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\Core\Entity\ContentEntityForm",
 *       "add" = "Drupal\Core\Entity\ContentEntityForm",
 *       "edit" = "Drupal\Core\Entity\ContentEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler"
 *   },
 *   admin_permission = "administer banner",
 *   base_table = "banner",
 *   data_table = "banner_data",
 *   revision_table = "banner_revision",
 *   revision_data_table = "banner_data_revision",
 *   translatable = TRUE,
 *   content_translation_ui_skip = TRUE,
 *   show_revision_ui = TRUE,
 *   entity_keys = {
 *     "id" = "banner_id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "title",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "published" = "status",
 *   },
 *   links = {
 *     "add-page" = "/banner/add",
 *     "add-form" = "/banner/add/{banner_type}",
 *     "edit-form" = "/banner/{banner}/edit",
 *     "delete-form" = "/banner/{banner}/delete",
 *     "collection" = "/admin/content/banner"
 *   },
 *   bundle_entity_type = "banner_type",
 *   field_ui_base_route = "entity.banner_type.edit_form",
 * )
 */
class Banner extends ContentEntityBase implements BannerInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getText() {
    return $this->get('text')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setText($text) {
    $this->set('text', $text);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMedia() {
    if ($media = !$this->get('media')->isEmpty() ? $this->get('media')->entity : NULL) {
      if (\Drupal::languageManager()->isMultilingual()) {
        /** @var \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository */
        $entity_repository = \Drupal::service('entity.repository');
        $media = $entity_repository->getTranslationFromContext($media);
      }
    }
    return $media;
  }

  /**
   * {@inheritdoc}
   */
  public function getImage() {
    $image = NULL;
    if ($this->hasImage()) {
      $media_entity = $this->getMedia();
      /** @var \Drupal\file\FileInterface $image */
      $image = $media_entity->get('field_media_image')->entity;
    }
    return $image;
  }

  /**
   * {@inheritdoc}
   */
  public function hasImage() {
    $media_entity = $this->getMedia();
    return !empty($media_entity) && $media_entity->getSource() instanceof Image;
  }

  /**
   * {@inheritdoc}
   */
  public function getVideo() {
    $video = NULL;
    if ($this->hasVideo()) {
      $media_entity = $this->getMedia();
      /** @var \Drupal\file\FileInterface $video */
      $video = $media_entity->get('field_media_video_file')->entity;
    }
    return $video;
  }

  /**
   * {@inheritdoc}
   */
  public function hasVideo() {
    $media_entity = $this->getMedia();
    return !empty($media_entity) && $media_entity->getSource() instanceof VideoFile;
  }

  /**
   * {@inheritdoc}
   */
  public function getCustomCssClasses() {
    return array_map(function ($field_value) {
      return $field_value['value'];
    }, $this->get('custom_css_classes')->getValue());
  }

  /**
   * {@inheritdoc}
   */
  public function setCustomCssClasses(array $custom_css_classes) {
    $this->set('custom_css_classes', $custom_css_classes);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function hasCustomCssClasses() {
    return !$this->get('custom_css_classes')->isEmpty();
  }

  /**
   * {@inheritdoc}
   */
  public function hasCustomCssClass($class) {
    if (!$this->hasCustomCssClasses()) {
      return FALSE;
    }
    $classes = $this->getCustomCssClasses();
    return in_array($class, $classes);
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setDescription(t('The banner author.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\banner\Entity\Banner::getCurrentUserId')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The banner title.'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSetting('default_value', '')
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['text'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Text'))
      ->setDescription(t('The banner text.'))
      ->setRequired(FALSE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => 1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['media'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Image/Video'))
      ->setSetting('target_type', 'media')
      ->setSetting('handler', 'default:media')
      ->setSetting('handler_settings', [
        'target_bundles' => [
          'image' => 'image',
          'video' => 'video',
        ],
        'sort' => ['field' => '_none'],
        'auto_create' => FALSE,
        'auto_create_bundle' => 'image',
      ])
      ->setTranslatable(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'media_library_widget',
        'weight' => 2,
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
        'weight' => 2,
        'label' => 'hidden',
        'settings' => [
          'view_mode' => 'banner',
          'link' => FALSE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['custom_css_classes'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Custom CSS classes'))
      ->setDescription(t('Custom CSS classes added to the banner parent element.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setPropertyConstraints('value', [
        'Regex' => [
          'pattern' => '/^[a-zA-Z0-9_\-]+$/',
          'message' => t('Please provide a valid CSS class name.'),
        ],
      ])
      ->setTranslatable(FALSE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 99,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time when the banner was created.'))
      ->setDisplayConfigurable('form', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time when the banner was last edited.'));

    $fields['status']
      ->setTranslatable(FALSE)
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

  /**
   * Default value callback for 'uid' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return array
   *   An array of default values.
   */
  public static function getCurrentUserId() {
    return [\Drupal::currentUser()->id()];
  }

}
