<?php

namespace Drupal\banner\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the banner type entity class.
 *
 * @ConfigEntityType(
 *   id = "banner_type",
 *   label = @Translation("Banner type"),
 *   label_singular = @Translation("Banner type"),
 *   label_plural = @Translation("Banner types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count banner type",
 *     plural = "@count banner types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\banner\Form\BannerTypeForm",
 *       "edit" = "Drupal\banner\Form\BannerTypeForm",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\banner\BannerTypeListBuilder",
 *   },
 *   config_prefix = "banner_type",
 *   admin_permission = "administer banner",
 *   bundle_of = "banner",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/banner-types/add",
 *     "edit-form" = "/admin/structure/banner-types/{banner_type}/edit",
 *     "collection" = "/admin/structure/banner-types"
 *   }
 * )
 */
class BannerType extends ConfigEntityBundleBase implements BannerTypeInterface {

  /**
   * The banner type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The banner type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The banner type description.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

}
