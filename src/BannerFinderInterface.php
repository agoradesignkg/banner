<?php

namespace Drupal\banner;

use Drupal\node\NodeInterface;

/**
 * The banner finder interface.
 */
interface BannerFinderInterface {

  /**
   * Loads banner entity IDs from the active node (based on the route context).
   *
   * @return int[]
   *   The banner entity IDs.
   */
  public function getBannerIdsFromActiveNode();

  /**
   * Finds and returns the active node, if available.
   *
   * @return \Drupal\node\NodeInterface|null
   *   The active node - based on the active route context - or NULL, if not
   *   found (non-node routes).
   */
  public function getActiveNode();

  /**
   * Loads banner entity IDs from the given node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node which banner ids should be extracted.
   * @param bool $filter_unpublished
   *   Whether to filter unpublished banner entities. Defaults to TRUE.
   *
   * @return int[]
   *   The banner entity IDs.
   */
  public function getBannerIdsFromNode(NodeInterface $node, bool $filter_unpublished = TRUE);

}
