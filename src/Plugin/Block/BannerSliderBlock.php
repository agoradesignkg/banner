<?php

namespace Drupal\banner\Plugin\Block;

use Drupal\banner\BannerFinderInterface;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides banner block.
 */
#[Block(
  id: "banner_slider_block",
  admin_label: new TranslatableMarkup("Banner slider block"),
  category: new TranslatableMarkup('banner'),
)]
class BannerSliderBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The banner finder.
   *
   * @var \Drupal\banner\BannerFinderInterface
   */
  protected BannerFinderInterface $bannerFinder;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected EntityDisplayRepositoryInterface $entityDisplayRepository;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected EntityRepositoryInterface $entityRepository;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * Constructs a BannerSliderBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\banner\BannerFinderInterface $banner_finder
   *   The banner finder.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, BannerFinderInterface $banner_finder, EntityDisplayRepositoryInterface $entity_display_repository, EntityRepositoryInterface $entity_repository, EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->bannerFinder = $banner_finder;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->entityRepository = $entity_repository;
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('banner.banner_finder'),
      $container->get('entity_display.repository'),
      $container->get('entity.repository'),
      $container->get('entity_type.manager'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'view_mode' => 'default',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('View mode'),
      '#description' => $this->t('The view mode that will be used for rendering the banner in the block.'),
      '#default_value' => $this->configuration['view_mode'],
      '#options' => $this->getAvailableViewModes(),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['view_mode'] = $form_state->getValue('view_mode');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    try {
      $items = $this->loadBannerEntitiesFromActiveNode();
    }
    catch (InvalidPluginDefinitionException $e) {
    }
    $output = [];
    if (!empty($items)) {
      $output['#theme'] = 'banner_slider';
      $output['#items'] = $items;
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['route']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $tags = parent::getCacheTags();
    $node = $this->bannerFinder->getActiveNode();
    if (!empty($node)) {
      $tags = Cache::mergeTags($tags, ['node:' . $node->id()]);
    }
    $tags = Cache::mergeTags($tags, ['banner_list']);
    return $tags;
  }

  /**
   * Loads banner entities as renderable array from active node.
   *
   * @return array
   *   A renderable array of banner entities (or empty).
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  protected function loadBannerEntitiesFromActiveNode() {
    $bannerEntities = [];
    $ids = $this->bannerFinder->getBannerIdsFromActiveNode();
    foreach ($ids as $id) {
      /** @var \Drupal\banner\Entity\BannerInterface $banner_entity */
      $banner_entity = $this->entityTypeManager
        ->getStorage('banner')
        ->load($id);

      if ($this->languageManager->isMultilingual()) {
        /** @var \Drupal\banner\Entity\BannerInterface $banner_entity */
        $banner_entity = $this->entityRepository->getTranslationFromContext($banner_entity);
        $langcode = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
        // Normally, Drupal falls back to the default language, but we want to
        // skip the slide, if the translation exists, but is unpublished.
        if ($banner_entity->hasTranslation($langcode) && $banner_entity->language()->getId() !== $langcode) {
          /** @var \Drupal\banner\Entity\BannerInterface $requested_translation */
          $requested_translation = $banner_entity->getTranslation($langcode);
          if (!$requested_translation->isPublished()) {
            continue;
          }
        }
      }

      $view_builder = $this->entityTypeManager->getViewBuilder('banner');
      $banner_renderable = $view_builder->view($banner_entity, $this->configuration['view_mode']);
      $bannerEntities[] = $banner_renderable;
    }
    return $bannerEntities;
  }

  /**
   * Gets available view modes of banner entities for block form configuration.
   */
  protected function getAvailableViewModes() {
    $options = [
      // Always add the 'default' view mode.
      'default' => 'Default',
    ];
    $form_modes = $this->entityDisplayRepository->getViewModes('banner');
    foreach ($form_modes as $id => $info) {
      $options[$id] = $info['label'];
    }
    return $options;
  }

}
