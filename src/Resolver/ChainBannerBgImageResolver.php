<?php

namespace Drupal\banner\Resolver;

use Drupal\banner\Entity\BannerInterface;

/**
 * Default implementation of the chain banner bg image resolver.
 */
class ChainBannerBgImageResolver implements ChainBannerBgImageResolverInterface {

  /**
   * The resolvers.
   *
   * @var \Drupal\banner\Resolver\BannerBgImageResolverInterface[]
   */
  protected $resolvers = [];

  /**
   * Constructs a new ChainBasePriceResolver object.
   *
   * @param \Drupal\banner\Resolver\BannerBgImageResolverInterface[] $resolvers
   *   The resolvers.
   */
  public function __construct(array $resolvers = []) {
    $this->resolvers = $resolvers;
  }

  /**
   * {@inheritdoc}
   */
  public function addResolver(BannerBgImageResolverInterface $resolver) {
    $this->resolvers[] = $resolver;
  }

  /**
   * {@inheritdoc}
   */
  public function getResolvers() {
    return $this->resolvers;
  }

  /**
   * {@inheritdoc}
   */
  public function resolve(BannerInterface $entity) {
    foreach ($this->resolvers as $resolver) {
      if ($resolver->applies($entity)) {
        return $resolver->resolve($entity);
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function resolveUri(BannerInterface $entity) {
    foreach ($this->resolvers as $resolver) {
      if ($resolver->applies($entity)) {
        return $resolver->resolveUri($entity);
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(BannerInterface $entity) {
    return TRUE;
  }

}
