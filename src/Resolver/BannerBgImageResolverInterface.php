<?php

namespace Drupal\banner\Resolver;

use Drupal\banner\Entity\BannerInterface;

/**
 * Defines the interface for banner entity background image resolvers.
 *
 * By default, we want to render the image attached to banner entities as
 * background image. But in some customer projects, we may want to override this
 * default rule, so we have built the resolver chain, to be flexible enough for
 * any requirements.
 */
interface BannerBgImageResolverInterface {

  /**
   * Resolves the background image url of a given banner entity.
   *
   * The result of this function can be directly used as relative URL in Twig
   * templates eg.
   *
   * @param \Drupal\banner\Entity\BannerInterface $entity
   *   The banner entity.
   *
   * @return string|null
   *   A string containing the background image URL, if resolved. An empty
   *   string or null can be returned in case the resolver is responsible for
   *   determining the background image, but couldn't find one, e.g. empty image
   *   field, etc.
   */
  public function resolve(BannerInterface $entity);

  /**
   * Resolves the background image uri of a given banner entity.
   *
   * The result of this function can be used for further processing with an
   * image style eg.
   *
   * @param \Drupal\banner\Entity\BannerInterface $entity
   *   The banner entity.
   *
   * @return string|null
   *   A string containing the background image file uri, if resolved. An empty
   *   string or null can be returned in case the resolver is responsible for
   *   determining the background image, but couldn't find one, e.g. empty image
   *   field, etc.
   */
  public function resolveUri(BannerInterface $entity);

  /**
   * Whether this resolver is responsible to decide on resolving a bg image.
   *
   * @param \Drupal\banner\Entity\BannerInterface $entity
   *   The banner entity.
   *
   * @return bool
   *   TRUE, if the resolver takes over responsibility to decide on resolving
   *   the background image url, even if finally none could be provided.
   *   Otherwise FALSE, indicating that the next resolver in the chain should be
   *   called.
   */
  public function applies(BannerInterface $entity);

}
