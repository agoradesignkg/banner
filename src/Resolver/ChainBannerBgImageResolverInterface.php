<?php

namespace Drupal\banner\Resolver;

/**
 * Runs the added resolvers one by one until one of them returns the bg image.
 *
 * Each resolver in the chain can be another chain, which is why this interface
 * extends the base banner background image resolver one.
 */
interface ChainBannerBgImageResolverInterface extends BannerBgImageResolverInterface {

  /**
   * Adds a resolver.
   *
   * @param \Drupal\banner\Resolver\BannerBgImageResolverInterface $resolver
   *   The resolver.
   */
  public function addResolver(BannerBgImageResolverInterface $resolver);

  /**
   * Gets all added resolvers.
   *
   * @return \Drupal\banner\Resolver\BannerBgImageResolverInterface[]
   *   The resolvers.
   */
  public function getResolvers();

}
