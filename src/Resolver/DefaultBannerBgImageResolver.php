<?php

namespace Drupal\banner\Resolver;

use Drupal\banner\Entity\BannerInterface;

/**
 * Returns the background image based on the banner bundle and a specific field.
 */
class DefaultBannerBgImageResolver implements BannerBgImageResolverInterface {

  /**
   * {@inheritdoc}
   */
  public function resolve(BannerInterface $entity) {
    $url = NULL;
    if (!empty($entity) && $entity->hasImage()) {
      $file = $entity->getImage();
      $url = $file->createFileUrl(TRUE);
    }
    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function resolveUri(BannerInterface $entity) {
    $url = NULL;
    if (!empty($entity) && $entity->hasImage()) {
      $file = $entity->getImage();
      $url = $file->getFileUri();
    }
    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(BannerInterface $entity) {
    // Formerly, the default BG image resolver always applied to a banner.
    // After changing to media references, we'll make it dependent on having a
    // a video or not. If the banner is completely empty, the resolver will also
    // apply therefore. This small difference will not matter in 99.9% of the
    // cases.
    return !$entity->hasVideo();
  }

}
