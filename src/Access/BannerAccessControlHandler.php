<?php

namespace Drupal\banner\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultReasonInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines an access control handler for the banner entity.
 */
class BannerAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    if ($account->hasPermission('administer banner')) {
      return AccessResult::allowed()->cachePerPermissions();
    }

    /** @var \Drupal\banner\Entity\BannerInterface $entity */
    $is_owner = ($account->id() && $account->id() === $entity->getOwnerId());
    switch ($operation) {
      case 'view':
        $access_result = AccessResult::allowedIf($account->hasPermission('view banner') && $entity->isPublished())
          ->cachePerPermissions()
          ->addCacheableDependency($entity);
        if (!$access_result->isAllowed()) {
          if ($access_result instanceof AccessResultReasonInterface) {
            $access_result->setReason("The 'view banner' permission is required and the banner item must be published.");
          }
        }
        return $access_result;

      case 'update':
        if ($account->hasPermission('update any banner')) {
          return AccessResult::allowed()->cachePerPermissions();
        }
        return AccessResult::allowedIf($account->hasPermission('update banner') && $is_owner)
          ->cachePerPermissions()
          ->cachePerUser()
          ->addCacheableDependency($entity);

      case 'delete':
        if ($account->hasPermission('delete any banner')) {
          return AccessResult::allowed()->cachePerPermissions();
        }
        return AccessResult::allowedIf($account->hasPermission('delete banner') && $is_owner)
          ->cachePerPermissions()
          ->cachePerUser()
          ->addCacheableDependency($entity);

      default:
        return AccessResult::neutral()->cachePerPermissions();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, [
      'administer banner',
      'create banner',
    ], 'OR');
  }

}
