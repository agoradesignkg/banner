<?php

namespace Drupal\banner;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Render controller for banner entities.
 */
class BannerViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function alterBuild(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
    /** @var \Drupal\banner\Entity\BannerInterface $entity */
    parent::alterBuild($build, $entity, $display, $view_mode);

    if ($entity->id()) {
      $build['#contextual_links']['banner'] = [
        'route_parameters' => ['banner' => $entity->id()],
        'metadata' => ['changed' => $entity->getChangedTime()],
      ];
    }

    if (!empty($build['custom_css_classes'])) {
      $build['custom_css_classes']['#access'] = FALSE;
    }
  }

}
