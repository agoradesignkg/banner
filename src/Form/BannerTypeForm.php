<?php

namespace Drupal\banner\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the banner type form.
 */
class BannerTypeForm extends BundleEntityFormBase {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\banner\Entity\BannerTypeInterface $banner_type */
    $banner_type = $this->entity;

    $form['#tree'] = TRUE;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $banner_type->label(),
      '#description' => $this->t('Label for the banner_type type.'),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $banner_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\banner\Entity\BannerType::load',
        'source' => ['label'],
      ],
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
    ];

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = $this->entity->save();
    $this->messenger()->addStatus($this->t('Saved the %label banner type.', ['%label' => $this->entity->label()]));
    $form_state->setRedirect('entity.banner.collection');
    return $result;
  }

}
