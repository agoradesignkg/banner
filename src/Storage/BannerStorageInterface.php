<?php

namespace Drupal\banner\Storage;

use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;

/**
 * Defines the interface for banner storage classes.
 */
interface BannerStorageInterface extends SqlEntityStorageInterface {

}
