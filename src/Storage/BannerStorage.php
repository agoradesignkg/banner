<?php

namespace Drupal\banner\Storage;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Default banner storage implementation.
 */
class BannerStorage extends SqlContentEntityStorage implements BannerStorageInterface {

}
