<?php

namespace Drupal\banner;

use Drupal\banner\Entity\BannerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * The default banner finder implementation.
 */
class BannerFinder implements BannerFinderInterface {

  const BANNER_FIELD_NAME = 'field_banner';

  /**
   * The active node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $activeNode;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a Bannerfinder object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   */
  public function __construct(ModuleHandlerInterface $moduleHandler, RequestStack $requestStack) {
    $this->moduleHandler = $moduleHandler;
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public function getBannerIdsFromActiveNode() {
    $ids = [];
    $node = $this->getActiveNode();
    if (!empty($node) && $node->access('view')) {
      $ids = $this->getBannerIdsFromNode($node);
    }

    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function getActiveNode() {
    if (empty($this->activeNode)) {
      $this->activeNode = $this->requestStack->getCurrentRequest()->attributes->get('node');
    }
    return $this->activeNode;
  }

  /**
   * {@inheritdoc}
   */
  public function getBannerIdsFromNode(NodeInterface $node, bool $filter_unpublished = TRUE) {
    $ids = [];

    $field_name = self::BANNER_FIELD_NAME;
    if ($node->hasField($field_name) && !$node->get($field_name)->isEmpty()) {
      $banner_entities = $node->get($field_name)->referencedEntities();
      foreach ($banner_entities as $banner) {
        assert($banner instanceof BannerInterface);
        if ($banner->isPublished() || !$filter_unpublished) {
          $ids[] = $banner->id();
        }
      }
    }

    // Allow other modules to modify the items.
    $this->moduleHandler->alter('banner_items', $ids);
    return $ids;
  }

}
