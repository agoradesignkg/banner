<?php

/**
 * @file
 * Post update functions for banner.
 */

/**
 * Transfer from image to media field.
 */
function banner_post_update_1() {
  $media_storage = \Drupal::entityTypeManager()->getStorage('media');
  /** @var \Drupal\banner\Entity\BannerInterface[] $banner_entities */
  $banner_entities = \Drupal::entityTypeManager()->getStorage('banner')->loadMultiple();
  foreach ($banner_entities as $banner) {
    if ($banner->hasField('image') && !$banner->get('image')->isEmpty()) {
      /** @var \Drupal\file\FileInterface $image_file */
      $image_file = $banner->get('image')->entity;
      $media_entity = $media_storage->create([
        'bundle' => 'image',
        'uid' => $image_file->getOwnerId(),
        'name' => $image_file->getFilename(),
        'status' => 1,
        'field_media_image' => [
          'target_id' => $image_file->id(),
        ],
      ]);
      $media_entity->save();
      $banner->set('media', $media_entity->id());
      $banner->set('image', NULL);
      $banner->save();
    }
  }
}
